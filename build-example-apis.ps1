# Generate runtime
& cargo run --bin cog-gen-c -- --api ./examples/runtime/api --headers ./examples/generated/headers/runtime --prefix ce_
& cargo run --bin cog-gen-rust -- --api ./examples/runtime/api --module ./examples/generated/runtime-api/src/generated

# Generate logger
& cargo run --bin cog-gen-c -- --api ./examples/plugin-logger/api --headers ./examples/generated/headers/logger --prefix ce_
& cargo run --bin cog-gen-rust -- --api ./examples/plugin-logger/api --module ./examples/generated/logger-api/src/generated