# Cog IDL

Cog IDL is an Interface Description Language for generating cross-language C APIs that can be
dynamically loaded.
It provides intentionally limited high level building blocks inspired by [*The Machinery*](https://ourmachinery.com/),
and generates bindings for various languages.

Currently supported languages:
- C
- Rust

Planned languages:
- C#
- Lua

Dynamic loading and hosting managed languages is out of scope of this project, it only provides API
generation infrastructure.


## License

Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
- MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
