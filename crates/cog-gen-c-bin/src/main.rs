use std::{
    fs::{create_dir_all, read_dir, remove_dir_all, write},
    path::{Path, PathBuf},
};

use clap::{App, Arg};

use {
    cog_gen_c::generate_header,
    cog_idl::{load_api_from_dir, Module},
};

fn main() {
    let matches = App::new("cog-gen-c")
        .version("0.1.0")
        .about("Generates C header files for a Cog-IDL API")
        .arg(
            Arg::with_name("api")
                .long("api")
                .value_name("DIRECTORY")
                .help("Sets the input directory containing .cog files")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("headers")
                .long("headers")
                .value_name("DIRECTORY")
                .help("Sets the output directory to write header files to")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("prefix")
                .long("prefix")
                .value_name("PREFIX")
                .help("Prepends all global namespace identifiers with a prefix")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("verbose")
                .long("verbose")
                .help("Sets logging mode to verbose, including more debugging information"),
        )
        .get_matches();

    let api = matches.value_of("api").unwrap();
    let headers = matches.value_of("headers").unwrap();
    let prefix = matches.value_of("prefix");
    let verbose = matches.is_present("verbose");

    prepare_headers_directory(headers);

    // Parse the API into memory
    let api = match load_api_from_dir(api) {
        Ok(api) => api,
        Err(err) => {
            println!("{}", err);
            return;
        }
    };

    if verbose {
        println!("{:#?}", api);
    }

    for module in &api.modules {
        generate_module(module, headers, prefix);
    }

    println!("Finished generating API");
}

fn prepare_headers_directory(headers: &str) {
    // Make sure the directory either doesn't exist or only contains header files
    if Path::new(headers).is_dir() {
        for path in read_dir(headers).unwrap() {
            let path = path.unwrap().path();

            if !path.is_file() {
                panic!("Headers directory must be empty or only contain header files");
            }

            if path.extension().and_then(|v| v.to_str()).unwrap_or("") != "h" {
                panic!("Headers directory must be empty or only contain header files");
            }
        }

        // Only delete after we're sure this is a valid directory
        remove_dir_all(headers).unwrap();
    }

    // At this point, it doesn't exist so create it
    create_dir_all(headers).unwrap();
}

fn generate_module(module: &Module, headers: &str, prefix: Option<&str>) {
    let mut header = PathBuf::from(headers);
    header.push(format!("{}.h", module.name));
    println!("Generating {}", header.display());

    let code = generate_header(module, prefix);

    // Write all the code to the header file
    write(header, code).unwrap();
}
