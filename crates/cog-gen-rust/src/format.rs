use std::fmt::Write;

use proc_macro2::{Delimiter, TokenStream, TokenTree};

pub fn format(code: &mut String, stream: TokenStream, indent: usize) {
    // TODO: Improve formatting by parsing what kind of context we're in and go into more specific
    // functions that handle conexts, like format_function, format_body.

    let mut need_ident_space = false;
    let mut need_literal_space = false;

    // This is a really ugly hack but it works for now
    let mut has_fn = false;
    let mut last_was_colon = false;

    for token in stream {
        match token {
            TokenTree::Ident(ident) => {
                if need_ident_space {
                    code.push(' ');
                }
                write!(code, "{}", ident).unwrap();

                if ident.to_string() == "fn" {
                    has_fn = true;
                }

                need_ident_space = true;
                need_literal_space = true;
                last_was_colon = false;
            }
            TokenTree::Punct(punct) => {
                let c = punct.as_char();

                if c == ',' || c == ';' {
                    write!(code, "{}\n", punct).unwrap();
                    for _ in 0..indent {
                        code.push_str("    ");
                    }

                    need_ident_space = false;
                    last_was_colon = false;
                } else if c == ':' {
                    write!(code, "{}", punct).unwrap();

                    // Double colons should not have a space after them
                    if last_was_colon {
                        need_ident_space = false;
                        last_was_colon = false;
                    } else {
                        need_ident_space = true;
                        last_was_colon = true;
                    }
                } else if c == '*' {
                    write!(code, "{}", punct).unwrap();

                    need_ident_space = false;
                    last_was_colon = false;
                } else {
                    write!(code, "{}", punct).unwrap();

                    need_ident_space = true;
                    last_was_colon = false;
                }

                need_literal_space = false;
            }
            TokenTree::Group(group) => {
                let (start, end) = match group.delimiter() {
                    Delimiter::Parenthesis => ('(', ')'),
                    Delimiter::Brace => ('{', '}'),
                    Delimiter::Bracket => ('[', ']'),
                    Delimiter::None => (' ', ' '),
                };

                if start == '{' {
                    code.push(' ');
                }

                code.push(start);

                let stream = group.stream();

                // Indent code blocks
                let indented = start == '{' || (has_fn && start == '(' && !stream.is_empty());
                if indented {
                    code.push('\n');
                    for _ in 0..(indent + 1) {
                        code.push_str("    ");
                    }
                }

                format(code, stream, indent + 1);

                // In an indented block, put the end on a new line as well
                if indented {
                    code.push('\n');
                    for _ in 0..indent {
                        code.push_str("    ");
                    }
                }

                code.push(end);

                // Parens are in-line, everything else finishes the line
                if end != ')' {
                    code.push('\n');
                    for _ in 0..indent {
                        code.push_str("    ");
                    }
                }

                need_ident_space = false;
                need_literal_space = false;
                last_was_colon = false;
            }
            TokenTree::Literal(literal) => {
                if need_literal_space {
                    code.push(' ');
                }
                write!(code, "{}", literal).unwrap();

                need_ident_space = true;
                need_literal_space = false;
                last_was_colon = false;
            }
        }
    }
}
