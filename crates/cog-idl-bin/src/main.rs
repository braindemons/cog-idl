use serde::{Serialize, Deserialize};

use cog_idl::{load_api_from_dir, load_module_from_str};

fn main() {
    // TODO: Add a mode to read the entire input as a file, using std::io::Read::read_to_string

    // Treat every line as a single command serialized in JSON
    loop {
        let mut buffer = String::new();
        let size = std::io::stdin().read_line(&mut buffer).unwrap();

        // If we got nothing just quit
        if size == 0 {
            return;
        }

        let command: Command = serde_json::from_str(&buffer).unwrap();

        let response: String = match command {
            Command::LoadModuleFromStr { name, idl } => {
                let result = load_module_from_str(name, &idl);

                match result {
                    Ok(module) => {
                        let response = Response {
                            success: true,
                            data: module,
                        };
                        serde_json::to_string(&response).unwrap()
                    }
                    Err(error) => {
                        let response = Response {
                            success: false,
                            data: error.to_string(),
                        };
                        serde_json::to_string(&response).unwrap()
                    }
                }
            }
            Command::LoadApiFromDir(directory) => {
                let result = load_api_from_dir(directory);
                match result {
                    Ok(api) => {
                        let response = Response {
                            success: true,
                            data: api,
                        };
                        serde_json::to_string(&response).unwrap()
                    }
                    Err(error) => {
                        let response = Response {
                            success: false,
                            data: error.to_string(),
                        };
                        serde_json::to_string(&response).unwrap()
                    }
                }
            }
        };

        println!("{}", response);
    }
}

#[derive(Deserialize, Debug)]
enum Command {
    LoadModuleFromStr { name: String, idl: String },
    LoadApiFromDir(String),
}

#[derive(Serialize)]
struct Response<T> {
    success: bool,
    data: T,
}
