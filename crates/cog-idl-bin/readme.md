# Cog IDL Binary

Command line interface for parsing Cog IDL files.


## Usage

The tool listens for commands on stdin, and sends out responses on stdout.
Every line in stdin is interpreted as a single command.
Commands and responses are in JSON.

To parse a module from a string:

```json
{
    "LoadModuleFromStr": {
        "name": "my_api",
        "idl": "interface foo {\n    fn bar();\n}"
    }
}
```

To parse an API from a directory:

```json
{
    "LoadApiFromDir": "../some/directory/api"
}
```