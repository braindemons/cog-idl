use {
    pest::{error::ErrorVariant, iterators::Pair, Parser},
    pest_derive::Parser,
};

use crate::{Function, Interface, Module, Parameter, Type, TypeModifier, TypeName};

// This include forces recompiling this source file if the grammar file changes.
#[cfg(debug_assertions)]
const _GRAMMAR: &str = include_str!("cog.pest");

#[derive(Parser)]
#[grammar = "parser/cog.pest"]
struct CogParser;

pub type ParseError = pest::error::Error<Rule>;

pub fn parse_module(name: String, text: &str) -> Result<Module, ParseError> {
    let mut pairs = CogParser::parse(Rule::cog, &text)?;

    let mut module = Module {
        name,
        documentation: None,
        interfaces: Vec::new(),
    };

    for pair in pairs.next().unwrap().into_inner() {
        match pair.as_rule() {
            Rule::interface => module.interfaces.push(parse_interface(pair)?),
            Rule::module_doc_comment => {
                module.documentation = Some(parse_doc_comment(pair));
            }
            Rule::EOI => {}
            rule => unreachable!("Rule {:?} should not be reachable", rule),
        }
    }

    Ok(module)
}

fn parse_interface(pair: Pair<Rule>) -> Result<Interface, ParseError> {
    let mut name = None;
    let mut documentation = None;
    let mut functions = Vec::new();

    for pair in pair.into_inner() {
        match pair.as_rule() {
            Rule::identifier => name = Some(pair.as_str().to_string()),
            Rule::doc_comment => documentation = Some(parse_doc_comment(pair)),
            Rule::function => functions.push(parse_function(pair)?),
            Rule::keyword_interface | Rule::brace_open | Rule::brace_close => {}
            rule => unreachable!("Rule {:?} should not be reachable", rule),
        }
    }

    Ok(Interface {
        name: name.unwrap(),
        documentation,
        functions,
    })
}

fn parse_function(pair: Pair<Rule>) -> Result<Function, ParseError> {
    let mut name = None;
    let mut documentation = None;
    let mut parameters = Vec::new();
    let mut return_type = None;

    for pair in pair.into_inner() {
        match pair.as_rule() {
            Rule::identifier => name = Some(pair.as_str().to_string()),
            Rule::doc_comment => documentation = Some(parse_doc_comment(pair)),
            Rule::parameter => parameters.push(parse_parameter(pair)?),
            Rule::type_ => {
                let span = pair.as_span();
                return_type = Some((parse_type(pair)?, span));
            },
            Rule::keyword_fn
            | Rule::paren_open
            | Rule::paren_close
            | Rule::semicolon
            | Rule::operator_arrow => {}
            rule => unreachable!("Rule {:?} should not be reachable", rule),
        }
    }

    // Return types can never be a reference, because the pointer lifetime is non-trivial
    if let Some((return_type, return_span)) = &return_type {
        if return_type.modifier == TypeModifier::Reference {
            let error = ErrorVariant::<Rule>::CustomError {
                message: format!(
                    "return types can't be a reference, return lifetimes are non-trivial"
                ),
            };
            return Err(ParseError::new_from_span(error, return_span.clone()));
        }
    }

    Ok(Function {
        name: name.unwrap(),
        documentation,
        parameters,
        return_type: return_type.map(|v| v.0),
    })
}

fn parse_parameter(pair: Pair<Rule>) -> Result<Parameter, ParseError> {
    let mut name = None;
    let mut type_ = None;

    for pair in pair.into_inner() {
        match pair.as_rule() {
            Rule::identifier => name = Some(pair.as_str().to_string()),
            Rule::type_ => type_ = Some(parse_type(pair)?),
            rule => unreachable!("Rule {:?} should not be reachable", rule),
        }
    }

    Ok(Parameter {
        name: name.unwrap(),
        type_: type_.unwrap(),
    })
}

fn parse_type(pair: Pair<Rule>) -> Result<Type, ParseError> {
    let pair_span = pair.as_span();
    let mut modifier = TypeModifier::None;
    let mut type_name = None;

    for pair in pair.into_inner() {
        match pair.as_rule() {
            Rule::operator_ptr => modifier = TypeModifier::Pointer,
            Rule::operator_ref => modifier = TypeModifier::Reference,
            Rule::identifier => type_name = Some((pair.as_str().to_string(), pair.as_span())),
            rule => unreachable!("Rule {:?} should not be reachable", rule),
        }
    }

    let (type_name, type_name_span) = type_name.unwrap();

    let type_name = match type_name.as_str() {
        "cstr" => TypeName::CString,
        "any" => TypeName::Any,
        _ => {
            let error = ErrorVariant::<Rule>::CustomError {
                message: format!("\"{}\" is not a known type", type_name),
            };
            return Err(ParseError::new_from_span(error, type_name_span));
        }
    };

    // Right now we don't support value-types yet
    if modifier == TypeModifier::None {
        let error = ErrorVariant::<Rule>::CustomError {
            message: format!("value types are not yet supported"),
        };
        return Err(ParseError::new_from_span(error, pair_span));
    }

    Ok(Type {
        modifier,
        type_name,
    })
}

fn parse_doc_comment(pair: Pair<Rule>) -> String {
    let mut text = String::new();

    for pair in pair.into_inner() {
        assert!(
            pair.as_rule() == Rule::doc_comment_line
                || pair.as_rule() == Rule::module_doc_comment_line
        );

        // Get the inner doc_comment_line_text
        let text_pair = pair.into_inner().next().unwrap();
        assert_eq!(text_pair.as_rule(), Rule::doc_comment_line_text);

        // Get the string data out of the comment
        text.push_str(text_pair.as_str().trim_start().trim_end());
        text.push('\n');
    }

    text.trim_start().trim_end().to_string()
}
