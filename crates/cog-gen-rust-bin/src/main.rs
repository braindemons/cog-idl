use std::{
    fmt::Write,
    fs::{create_dir_all, read_dir, remove_dir_all, write},
    path::{Path, PathBuf},
    process::Command,
};

use clap::{App, Arg};

use {
    cog_gen_rust::{generate_module, generate_module_formatted, write_generated_header},
    cog_idl::load_api_from_dir,
};

fn main() {
    let matches = App::new("cog-gen-rust")
        .version("0.1.0")
        .about("Generates Rust module files for a Cog-IDL API.")
        .arg(
            Arg::with_name("api")
                .long("api")
                .value_name("DIRECTORY")
                .help("Sets the input directory containing .cog files.")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("module")
                .long("module")
                .value_name("DIRECTORY")
                .help("Sets the output module directory to write module files to.")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("formatter")
                .long("formatter")
                .possible_values(&["rustfmt", "internal"])
                .default_value("rustfmt")
                .help("Sets the formatter to be used on output code."),
        )
        .arg(
            Arg::with_name("verbose")
                .long("verbose")
                .help("Sets logging mode to verbose, including more debugging information."),
        )
        .get_matches();

    let api = matches.value_of("api").unwrap();
    let module_dir = matches.value_of("module").unwrap();
    let formatter = matches.value_of("formatter").unwrap();
    let _verbose = matches.is_present("verbose");

    prepare_module_directory(module_dir);

    // Parse the API into memory
    let api = match load_api_from_dir(api) {
        Ok(api) => api,
        Err(err) => {
            println!("{}", err);
            return;
        }
    };

    let mut root_module_code = String::new();
    write_generated_header(&mut root_module_code);

    // Generate all the modules
    for module in &api.modules {
        let mut module_file = PathBuf::from(module_dir);
        module_file.push(format!("{}.rs", module.name));

        println!("Generating {}", module_file.display());

        let module_code = if formatter == "rustfmt" {
            generate_module(module)
        } else {
            generate_module_formatted(module)
        };

        write(&module_file, module_code).unwrap();

        if formatter == "rustfmt" {
            run_rustfmt(&module_file);
        }

        writeln!(root_module_code, "pub mod {};", module.name).unwrap();
    }

    // Generate the root module
    let mut module_file = PathBuf::from(module_dir);
    module_file.push("mod.rs");
    write(module_file, root_module_code).unwrap();
}

fn prepare_module_directory(module_dir: &str) {
    // Make sure the directory either doesn't exist or only contains header files
    if Path::new(module_dir).is_dir() {
        for path in read_dir(module_dir).unwrap() {
            let path = path.unwrap().path();

            if !path.is_file() {
                panic!("Module directory must be empty or only contain module files");
            }

            if path.extension().and_then(|v| v.to_str()).unwrap_or("") != "rs" {
                panic!("Module directory must be empty or only contain module files");
            }
        }

        // Only delete after we're sure this is a valid directory
        remove_dir_all(module_dir).unwrap();
    }

    // At this point, it doesn't exist so create it
    create_dir_all(module_dir).unwrap();
}

fn run_rustfmt(file: &PathBuf) {
    let output = Command::new("rustfmt")
        .arg(file)
        .output()
        .expect("Failed to run rustfmt, it may not be installed");

    if !output.status.success() {
        println!(
            "rustfmt failed:\n{}",
            String::from_utf8_lossy(&output.stderr)
        );
    }
}
