#include <windows.h>
#include <stdio.h>
#include <string>
#include <unordered_map>

#include <runtime/registry.h>

typedef void (__cdecl *LOADPROC)(ce_registry *registry);

static std::unordered_map<std::string, void*> apis;

void* get(char *name) {
    std::string namestr(name);
    return apis.at(namestr);
}

void set(char *name, void *api) {
    std::string namestr(name);
    apis.insert({namestr, api});
}

void try_load(LPCSTR library_name, ce_registry *registry) {
    HINSTANCE hinst_plugin = LoadLibrary(library_name);

    if (hinst_plugin != NULL) {
        LOADPROC load_proc = (LOADPROC)GetProcAddress(hinst_plugin, "load");

        if (load_proc != NULL) {
            load_proc(registry);
        } else {
            printf("Plugin does not contain load function\n");
        }
    }
}

int main() {
    apis = std::unordered_map<std::string, void*>();

    // Create the registry implementation
    ce_registry registry;
    registry.get = get;
    registry.set = set;

    // Try to load the example libraries
    try_load("plugin-logger.dll", &registry);
    try_load("plugin-hello-c.dll", &registry);
    try_load("plugin-hello-rs.dll", &registry);

    return 0;
}
