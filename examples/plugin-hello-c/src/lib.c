#include <runtime/registry.h>
#include <logger/logger.h>

#define DLLEXPORT __declspec(dllexport)

#ifdef __cplusplus
extern "C" {
#endif

DLLEXPORT void load(ce_registry *registry);

#ifdef __cplusplus
}
#endif

DLLEXPORT void load(ce_registry *registry) {
    ce_logger *logger = registry->get("logger");
    logger->hello_world("Plugin Hello C");
}
