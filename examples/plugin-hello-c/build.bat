@echo off

REM Configuring build environment
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64

REM Building runtime
mkdir bin
cl ./src/lib.c /LD /I"../generated/headers" /Fo".\bin\\" /Fe".\bin\plugin-hello-c.dll"