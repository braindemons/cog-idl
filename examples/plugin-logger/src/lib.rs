use std::{cell::RefCell, ffi::{CStr, CString}};

use {logger_api::logger::Logger, runtime_api::registry::Registry};

thread_local! {
    pub static LOGGER: RefCell<Option<Logger>> = RefCell::new(None);
}

#[no_mangle]
pub unsafe extern "C" fn load(registry: &Registry) {
    let logger = Logger {
        hello_world: hello_world,
    };

    let ptr = LOGGER.with(|f| {
        let mut option = f.borrow_mut();
        *option = Some(logger);
        option.as_ref().unwrap() as *const _ as *const std::ffi::c_void
    });

    let name = CString::new("logger").unwrap();
    registry.set(&name, ptr);
}

unsafe extern "C" fn hello_world(source: *const std::os::raw::c_char) {
    let source = CStr::from_ptr(source);
    println!("Hello from \"{}\"!", source.to_string_lossy());
}
