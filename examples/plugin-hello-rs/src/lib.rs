use std::ffi::CString;

use {logger_api::logger::Logger, runtime_api::registry::Registry};

#[no_mangle]
pub unsafe extern "C" fn load(registry: &Registry) {
    let api_name = CString::new("logger").unwrap();
    let api = registry.get(&api_name) as *const Logger;

    let text = CString::new("Plugin Hello Rust").unwrap();
    (*api).hello_world(&text);
}
